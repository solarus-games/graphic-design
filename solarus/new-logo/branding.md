# Branding

## Brand colors

- `#ff7b00` (vibrant orange)
- `#ffab00` (yellowish orange)
- `#2e1b66` (dark purple)
- `#4a2c94` (light purple)

## Fonts

- Text: Inter
- Headings: Ubuntu Titling
- Code: Fira Code
