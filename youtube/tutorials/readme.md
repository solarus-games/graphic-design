# Template for Solarus Video Tutorial thumbnails

1. Please ensure that you have *UbuntuTitling-Bold* installed. This font is free, under the GNU GPL v3 license. You can download it on multiple sites, for instance [FontSquirrel](https://www.fontsquirrel.com/fonts/ubuntu-titling).

2. Use the Python (>3) script to generate a JPEG thumbnail.
    * The image will be **2450*1440 pixels**, because this is the recommended size by Youtube, in order to be displayed from tiny (small phones) to large screens (4K TVs).
    * File naming is automatic: `solarus_tutorial_thumbnail_<number>_<lang>.jpeg` where `<number>` is the chapter number and `<lang>` is the language (`"fr"` or `"en"`).

```bash
python thumbnail_generator.py -number 42 -title "Tutorial chapter title (auto word-wrap)" -l fr -o "path/to/output/folder"
```

Result (resized):

![Thumbnail generator example](solarus_tutorial_thumbnail_example.jpeg)
