![Icon](icon.png)

# Graphic design for Solarus projects

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

You will find here the source files of the pictures made for Solarus (Adobe Illustrator or Adobe Photoshop), i.e logos and other visual communication medias, as well as SVG and PNG exports.

NB: no game-related content here (i.e. no sprites, tilesets, etc.)

> **Note:** The press kit (logos, screenshots) can be found [on the website](https://solarus-games.org/about/press-kit);

## Colors

| Name             |   Value   | Role                       |
| :--------------- | :-------: | :------------------------- |
| Yellowish Orange | `#ffab00` | Primary color              |
| Orange           | `#ff7b00` | Secondary color            |
| Purple           | `#4a2c94` | Main background color      |
| Dark Purple      | `#2e1b66` | Secondary background color |

## Fonts

| Role           | Font                |
| :------------- | :------------------ |
| Headlines      | Ubuntu Titling Bold |
| Regular Text   | Inter Regular       |
| Monospace Text | Fira Code           |
